FROM alpine:3.7

RUN apk add --no-cache nginx && \
    mkdir -p /run/nginx && \
    mkdir /www && \
    touch /run/nginx/nginx.pid

COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY index.html /www/

ENTRYPOINT ["nginx", "-g", "daemon off;"]